using namespace std;
#include <iostream>
#include <string>

int main() {
    string name;
    int a, b, larger, smaller;

    cout << "Jak sie nazywasz?" << endl;
    cin >> name;


    cout << "Witaj " + name + ", podaj dowolna liczbe" << endl;
    cin >> a;
    cout << "podaj kolejna liczbe"  << endl;
    cin >> b;

    if (a == b) {
        cout << "podales dwie identyczne liczby";
        return 1;
    }

    if (a < b) {
        smaller = a;
        larger = b;
    } else {
        smaller = b;
        larger = a;
    }

    cout << "ilosc ujemnych liczb to: " + to_string((a < 0 ? 1 : 0) + (b < 0 ? 1 : 0) ) << endl;

    if (larger == 0) {
        cout << "dzielenie przez 0" << endl;
    } else {
        cout << "wynik dzielenia mniejszej wartosci przez dwokrotnosc wiekszej to: " + to_string((double)smaller / (2 * (double)larger)) << endl;
    }

    cout << "ilosc cyfr w mniejszej liczbie to: ";

    int total = 0;
    while(smaller != 0) {
        smaller = smaller / 10;
        total++;
    }

    cout << total << endl;

    return 0;
}
