# Zadanie 1 – z if-ami (0.75 pkt)

Napisać program, który:

1. Pyta użytkownika o imię, wczytuje je i drukuje powitanie z wykorzystaniem tego imienia.
2. Wczytuje dwie dowolne liczby całkowite do zmiennych typu int.
3. Drukuje ilość wczytanych liczb ujemnych.
4. Drukuje wynik dzielenia mniejszej liczby przez podwojoną wartość większej.
5. Drukuje informację, czy mniejsza z wczytanych liczb jest jednocyfrowa, czy
dwucyfrowa, czy też ma więcej cyfr.

UWAGA: jeśli wczytane liczby są sobie równe, należy zasygnalizować to odpowiednim
tekstem i niczego więcej nie robić. W tym celu można od razu skończyć program za pomocą
instrukcji return 0; wtedy dalsze polecenia nie będą się już wykonywać.

Wskazówki

1. Nie wolno dopuścić do dzielenia przez zero (wydrukować odpowiedni komunikat).
2. Nie wolno też dopuścić do obcinania po przecinku wyniku dzielenia – wynik musi być
dokładny.

Uwagi:

1. Przy drukowaniu tekstów i wprowadzaniu znaków nie używamy polskich znaków
diakrytycznych (tzw. ogonkowych), aby na wydrukach nie powstały „krzaczki” - natomiast
dopuszczalne są polskie znaki ogonkowe w komentarzach.
2. Pisanie komentarzy w programach nie jest konieczne, chyba że pomaga autorowi
w uporządkowaniu programu.
